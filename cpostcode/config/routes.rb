Rails.application.routes.draw do

  resources :subcontractors
  root 'static_pages#home'

end
