class CreateSubcontractors < ActiveRecord::Migration[5.1]
  def change
    create_table :subcontractors do |t|
      t.string :name
      t.float :lat
      t.float :lon

      t.timestamps
    end
  end
end
