// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs
//= require turbolinks
//= require_tree 

var originIcon;
var destinationIcon;
var cityCircle;
var map;
var origin;
var dests = [];
var placeNames = [];
var maxDest = 0;
var markersArray = [];


function initMap() {
  var manch = {lat: 53.486936, lng: -2.241317};
  map = new google.maps.Map(document.getElementById('map'), {
    zoom: 8,
    center: manch 
  });

  var geocoder = new google.maps.Geocoder();

  document.getElementById('submit').addEventListener('click', function() {
    geocodeAddress(geocoder, map);
  });


  originIcon = 'https://chart.googleapis.com/chart?' +
      'chst=d_map_pin_icon_withshadow&chld=home|FF0000|000000';


}

function geocodeAddress(geocoder, resultsMap) {
  var address = document.getElementById('address').value;
  var radius = document.getElementById('circle').value;
  geocoder.geocode({'address': address}, function(results, status) {
    if (status === 'OK') {

      origin = results[0].geometry.location;

      resultsMap.setCenter(origin);

      if (cityCircle != null) {
        cityCircle.setMap(null);
      }

      cityCircle = new google.maps.Circle({
        strokeColor: '#FF0000',
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: '#FF0000',
        fillOpacity: 0.35,
        map: resultsMap,
        center: results[0].geometry.location,
        radius: parseFloat(radius) * 1000
      });

      var service = new google.maps.DistanceMatrixService;
      var bounds = new google.maps.LatLngBounds;

      service.getDistanceMatrix({
        origins:[origin],
        destinations: dests,
        travelMode: 'DRIVING',
        unitSystem: google.maps.UnitSystem.METRIC,
        avoidHighways: false,
        avoidTolls: false
      }, function(response, status) {
        if (status !== 'OK') {
          alert('Error was: ' + status);
        } else {
          var originList = response.originAddresses;
          var destinationList = response.destinationAddresses;
          var outputDiv = document.getElementById('response');
          outputDiv.innerHTML = '';
          deleteMarkers(markersArray);

          var showGeocodedAddressOnMap = function(asDestination) {
            var icon = asDestination ? destinationIcon : originIcon;
            return function(results, status) {
              if (status === 'OK') {
                map.fitBounds(bounds.extend(results[0].geometry.location));
                markersArray.push(new google.maps.Marker({
                  map: map,
                  position: results[0].geometry.location,
                  icon: icon
                }));
              } else {
                alert('Geocode was not successful due to: ' + status);
              }
            };
          };

            order = 0;
            outputDiv.innerHTML += '<h2>Results</h2>';
            var results = response.rows[0].elements;
            geocoder.geocode({'address': originList[0]},
                showGeocodedAddressOnMap(false));
            for (var i = 0; i < results.length; i++) {
              console.log('dist: ' + results[i].distance.value + ' radius: '
                          + parseFloat(radius) * 1000);
              if(parseInt(results[i].distance.value) <= parseFloat(radius) 
                 * 1000) {

                order++;
                destinationIcon = 'https://chart.googleapis.com/chart?' +
                                  'chst=d_map_pin_letter_withshadow&chld=' + 
                                  order + '|2222FF|000055'; 
                geocoder.geocode({'address': destinationList[i]},
                  showGeocodedAddressOnMap(true));
                outputDiv.innerHTML += '<p>' + order + ": " + placeNames[i] +
                   ': ' + results[i].distance.text + ' away</p>';
              }
            }
          
        }
      });

    } else {
      alert('Geocode was not successful for the following reason: ' + status);
    }
  });
}

function updateLocations(name, lat, lng) {
  dests[maxDest] = {lat, lng};
  placeNames[maxDest] = name;
  console.log(dests[maxDest]);
  maxDest++;
}

function deleteMarkers(markersArray) {
  for (var i = 0; i < markersArray.length; i++) {
    markersArray[i].setMap(null);
  }
  markersArray = [];
}




