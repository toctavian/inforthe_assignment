json.extract! subcontractor, :id, :name, :lat, :lon, :created_at, :updated_at
json.url subcontractor_url(subcontractor, format: :json)
